package speechless.com;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.scheduler.BukkitRunnable;

public class RecoverBlockTask  extends BukkitRunnable {
    private Location loc;
    private BlockData block_data;
    RecoverBlockTask(Location loc, BlockData block_data){
        this.loc = loc;
        this.block_data = block_data;
        this.runTaskLater(Domain.plugin,1L);
    }
    @Override
    public void run() {
        loc.getBlock().setBlockData(block_data);
        for (Entity e : loc.getChunk().getEntities()
             ) {
            if(e instanceof Item && e.getLocation().distance(e.getLocation()) <1){
                e.remove();
            }
        }
    }
}
