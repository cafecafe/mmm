package speechless.com;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import java.util.List;

public class Command   {
    public static class ShowDomain implements CommandExecutor{
        @Override
        public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
            if( domainBlock.PlayerDomainList.get(commandSender.getName()).size() == 0)
                commandSender.sendMessage(ChatColor.RED+"You do not have the domain");
            for (domainBlock block: domainBlock.PlayerDomainList.get(commandSender.getName())
                 ) {
                commandSender.sendMessage(block.getName());
            }
           return true;
        }
    }

//    public static class CreateTeam implements CommandExecutor{
//        @Override
//        public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
//            TeamManagement.registerTeam(commandSender.getName());
//            commandSender.sendMessage("You create successfully!!");
//            return true;
//        }
//    }

    public static class Invite implements CommandExecutor{
        @Override
        public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
            List<Entity> entities = Bukkit.getPlayer(commandSender.getName()).getNearbyEntities(5, 5, 5);
            if(!TeamManagement.isTeamMember(commandSender.getName())){
                TeamManagement.registerTeam(commandSender.getName());
            }
            else if (entities.size() == 0) {
                commandSender.sendMessage(ChatColor.RED + "The player invited should be nearly!");
                return true;
            }
            commandSender.sendMessage(ChatColor.YELLOW+"You invite "+entities.size()+" entities!");
            for (Entity en : entities
            ) {
                String receiver = en.getName();
                if (Bukkit.getServer().getPlayer(receiver)!=null) {
                    if(TeamManagement.isTeamMember(receiver)) {
                        Bukkit.getServer().getPlayer(receiver).sendMessage(ChatColor.YELLOW+commandSender.getName()+" want invite you,but you have team");
                        continue;
                    }
                    TeamManagement.invitePlayer(receiver,commandSender.getName());
                    Bukkit.getServer().getPlayer(receiver).sendMessage(ChatColor.YELLOW + commandSender.getName() + " invite you to join team");

                }
            }
            return true;
        }
    }
    public static class AgreedTeam implements CommandExecutor{
        @Override
        public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
            if(strings.length == 0) {
                commandSender.sendMessage(ChatColor.RED+"Please type the inviter name");
                return false;
            }
            String sender = strings[0];
            if(Bukkit.getPlayer(sender)!=null) {
                TeamManagement.agreedInvitation(commandSender.getName(),sender);
            }
            return true;
        }
    }
    public static class QuitTeam implements CommandExecutor{
        @Override
        public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
            TeamManagement.quitTeam(commandSender.getName());
            return true;
        }
    }
    public static class ShowTeam implements CommandExecutor{
        @Override
        public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
            if(TeamManagement.isTeamMember(commandSender.getName())){
                TeamManagement.showTeam(commandSender.getName());
            }
            else{
                commandSender.sendMessage(ChatColor.RED+"You are not team member!");
            }
            return true;
        }
    }

    public static class Send implements CommandExecutor{
        @Override
        public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
           List<Entity> entities = Bukkit.getPlayer(commandSender.getName()).getNearbyEntities(5,5,5);
            if(strings.length == 0) {
                commandSender.sendMessage(ChatColor.RED+"Please ,type message!");
            }
            else if(entities.size()==0){
                commandSender.sendMessage(ChatColor.RED+"There are nothing to say!");
            }
            else{
                String message = strings[0];
                for (Entity en: entities
                ) {
                    if( Bukkit.getPlayer(en.getName())!=null)
                        Bukkit.getPlayer(en.getName()).sendMessage(ChatColor.YELLOW+commandSender.getName()+": "+ChatColor.WHITE+message);
                }
            }
            return true;
        }
    }

    public static class ResetScoreboard implements CommandExecutor {

        @Override
        public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
            main.resetBoardObjective();
            main.resetTeam();
            main.createTeam();;
            main.createBoardObjective();
            return true;
        }
    }
    public static class ResetSpiral implements CommandExecutor {

        @Override
        public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
            Task.spiralBuildTask.cancel();
            return true;
        }
    }
}
