package speechless.com;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import table.Table;

import java.util.*;

public class domainBlock {
    private final double center_x;
    private final double center_z;
    private final float range;
    private String owner = "";
    private String block_name;
    private Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
    private  Objective board_invade ;
    private  Objective board_score ;
    private  Objective main_invade ;
    static int num =0;
    static Map<String, Set<domainBlock>> PlayerDomainList = new HashMap<>();
    static Map<Integer,domainBlock> BlockList = new HashMap<>();
    static Set<domainBlock> Domains = new HashSet<>();
    private Set<String> Invader = new HashSet<>();
    private Set<String> Friend = new HashSet<>();
    private Set<String> Master = new HashSet<>();
    static int id =0;
    private int block_id ;
    private Table.Block_Table table =  main.block_table;
    domainBlock(double x,double z,int size) {
        this.center_x = x;
        this.center_z = z;
        this.range = size/2;
        id++;
        block_name = "domain"+ id;
        block_id = id;
        board_score = board.getObjective("score")==null ? board.registerNewObjective("score","dummy")
                 :board.getObjective("score");
        board_invade = board.getObjective(block_name)==null ? board.registerNewObjective(block_name,"dummy")
                 : board.getObjective(block_name);
        main_invade = main.board.getObjective(block_name)==null ? main.board.registerNewObjective(block_name,"dummy") :
                main.board.getObjective(block_name);
        block_name = block_name;
        Domains.add(this);
        block_name = block_name;
        BlockList.put(block_id,this);
        owner = table.getOwner(""+block_id);
        if(owner != ""){
            board_invade.setDisplayName(ChatColor.BLUE+owner);
            DomainListSet(owner,this);
        }else{
            board_invade.setDisplayName(ChatColor.GRAY+"FREE");
        }
    }
    public boolean isOnRange(Location loc){
        double x = loc.getX();
        double z = loc.getZ();
        return x <= range + center_x && x >= -range + center_x && z <= range + center_z && z >= -range + center_z;
    };
    public void renderBlockTeam(){
        for (Player player:Bukkit.getOnlinePlayers()
             ) {
            String id = TeamManagement.getTeamId(player.getName());
            if(id ==null) return;
            if(board.getTeam(id) !=null){
                board.getTeam(id).addEntry(player.getName());
            }
            else{
                board.registerNewTeam(id).addEntry(player.getName());
            }
            board.getTeam(id).setOption(Team.Option.NAME_TAG_VISIBILITY,Team.OptionStatus.NEVER);
            if(id != "default") {
                player.setDisplayName(ChatColor.YELLOW + "" + id + ":" + ChatColor.WHITE + player.getName());
                board.getTeam(id).setPrefix(ChatColor.GRAY+id+" ");
                board.getTeam(""+id).setAllowFriendlyFire(false);
                board.getTeam(""+id).setCanSeeFriendlyInvisibles(true);
            }

        }
    }

    public boolean isAfford(Player player){
        if(owner.equals("") ) return true;
        if(player.getName().equals(owner) || TeamManagement.isSameTeam(owner,player.getName())) return false;
        if(PlayerDomainList.containsKey(owner)&& PlayerDomainList.get(owner).size() > Domain.player_max_domain) return true;
        if(Bukkit.getPlayer(owner)!=null){
            return main.board.getObjective("player_score").getScore(player.getName()).getScore()
                    >= main.board.getObjective("player_score").getScore(owner).getScore();
        }
        else{
            return main.board.getObjective("player_score").getScore(player.getName()).getScore()
                    >= main.score_table.getScore(owner);
        }


    }
    public void setInvade(String name){
        if( !Invader.contains(name))
            Invader.add(name);
    }
    public void setOwner(Player player){
        Set<domainBlock> blocks;
        String name = player.getName();
        if(owner != "")
            PlayerDomainList.get(owner).remove(this);
        owner = name;
        table.registerOwner(""+block_id,owner);
        board_invade.setDisplayName(ChatColor.BLUE+owner);
        num++;
        DomainListSet(owner,this);
        main_invade.getScore(owner).setScore(0);
        for (Player man: Bukkit.getOnlinePlayers()
             ) {
            if(man.getName() == name) {
                player.sendMessage(ChatColor.GREEN+" Occupy successfully!!^_^");
            }
            else{
                man.sendMessage(ChatColor.RED+name+" ,have occupied Domain "+block_id+"( x:"+center_x+", z:"+center_z+")");
            }
        }
    }
    public static void DomainListSet(String name, domainBlock block){
        Set<domainBlock> blocks;
        if(!PlayerDomainList.containsKey(name)){
            blocks = new HashSet<domainBlock>();
            PlayerDomainList.put(name,blocks);
        }
        else{
            blocks = PlayerDomainList.get(name);
        }
        blocks.add(block);
    }
    public void setPoint(String player, int s){
        main_invade.getScore(player).setScore(s);
        board_invade.getScore(player).setScore(s);
    }
    public int getPoint(Player player){
        return main_invade.getScore(player.getName()).getScore();
    }
    public void DisplayBoard(Player player){
        player.setScoreboard(board);
        board_invade.setDisplaySlot(DisplaySlot.SIDEBAR);
        board_score.setDisplaySlot(DisplaySlot.PLAYER_LIST);
        detectClearTeamDisplay();
    }
    static void clearPlayerDomain(String name){
        if(PlayerDomainList.get(name) == null){
            return;
        }
        for (domainBlock block: PlayerDomainList.get(name)
             ) {
            block.free();
            num--;
        }

    }

    private void free() {
        board_invade.setDisplayName(ChatColor.GRAY+"FREE");
        table.registerOwner(""+block_id,"");
        owner = "";
    }

    public static int used_size(){
        return num;
    }
    public String getName(){
        return block_name;
    }
//    public double geC_x(){
//        return center_x;
//    }
//    public double geC_z(){
//        return center_z;
//    }
    public static void spawnFireworks(Location location, int amount){
        Location loc = location;
        loc.setY(loc.getY()+8);
        Firework fw = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();

        fwm.setPower(2);
        fwm.addEffect(FireworkEffect.builder().withColor(Color.LIME).flicker(true).build());

        fw.setFireworkMeta(fwm);
        fw.detonate();

        for(int i = 0;i<amount; i++){
            Firework fw2 = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
            fw2.setFireworkMeta(fwm);
        }
    }
    public void setBoard_score(String name,int i){

        board_score.getScore(name).setScore(i);
        main.board.getObjective("player_score").getScore(name).setScore(i);
    }
    public void BlockRecover(BlockBreakEvent event, Block block){
        Player player = event.getPlayer();
        if (owner.equals(player.getName())
                || isAfford(player)) return;
//        Objective obj = MyListener.playScoreListener.board_obj;
//        obj.getScore(player.getName() ).setScore(obj.getScore(player.getName() ).getScore()-penalty);
        int drop_exp = event.getExpToDrop();
        player.setExp(player.getExp() - drop_exp);
        new RecoverBlockTask(block.getLocation(),block.getBlockData());
        player.sendMessage(ChatColor.RED+"Illegal in domain!!"+block_id);
    }
    public void BlockRemove(Player player,Location loc){
        if (owner.equals(player.getName())
                || isAfford(player)  ) return;
        new RemoveBlockTask(loc);
        player.sendMessage(ChatColor.RED+"Illegal in domain!!"+block_id);
    }
    public void detectFooterDisplay(Player player){
        DisplayBoard(player);
        if(player.getName().equals(owner) ){
            player.setPlayerListFooter(ChatColor.GRAY+"Domain "+block_id+ChatColor.GREEN+"\n~Own~");
            toFriend(player.getName());
        }
        else if(TeamManagement.isSameTeam(player.getName(),owner)){
            player.setPlayerListFooter(ChatColor.GRAY+"Domain "+block_id+ChatColor.GREEN+"\n~belong your team~");
            toFriend(player.getName());
        }
        else if (isAfford(player)) {
            player.setPlayerListFooter(ChatColor.GRAY+"Domain "+block_id+ChatColor.YELLOW+"\nInvading...");
            toInvader(player.getName());

        }else if(!isAfford(player)){
            int own_point = main.board.getObjective("player_score").getScore(owner).getScore();
            player.setPlayerListFooter(ChatColor.RED+"!! NOT AVAILABLE !! QQ"
                    +ChatColor.YELLOW+"\n owner:"+ChatColor.BLUE+owner
                    +ChatColor.AQUA+"\n Need: "+own_point);
            setPoint(player.getName(),0);
            player.setScoreboard(main.board);
            clearMember(player.getName());
        }
    }
    public void detectSendMs(Player player){
        if(TeamManagement.isSameTeam(player.getName(),owner)){
            player.sendMessage(ChatColor.GREEN+"~You are on your team menber domain~");
        }
        else if(player.getName().equals(owner) ){
            player.sendMessage(ChatColor.GREEN+"~You are on your domain~");
        }
        else if (isAfford(player)) {
            player.sendMessage(ChatColor.YELLOW+"You are invading the domain "+block_id);

        }else if(!isAfford(player)){
            player.sendMessage(ChatColor.RED+"You are not able to occupy !!");
        }
    }
    public int getCenterDistance(Location loc){
        double dx =Math.abs(loc.getX() - center_x);
        double dz =Math.abs(loc.getZ() - center_z);
        return (int)Math.sqrt((int)(dx*dx) + (int)(dz*dz));
    }

    public void clearMember(String name) {
        Invader.remove(name);
        Friend.remove(name);
    }
    public void toFriend(String name){
        Invader.remove(name);
        Friend.add(name);
    }
    public  void toInvader(String name){
        Friend.remove(name);
        Invader.add(name);
    }

    public void detectClearTeamDisplay() {
        if(owner =="" ) return;
        if(Invader.size() ==0) {
            for ( String name : Friend.toArray(new String[Friend.size()])
            ) {
                Player player = Bukkit.getPlayer(name);
                if(isOnRange(player.getLocation())){
                    player.setScoreboard(main.board);
                }
            }
        }
    }

    public void sendPosition(Player man) {
        for (String player: Friend
             ) {
            int x =(int)man.getLocation().getX();
            int y = (int)man.getLocation().getY();
            int z = (int)man.getLocation().getZ();
            Bukkit.getPlayer(player).sendMessage(ChatColor.YELLOW+ man.getName()+" is in X:"+x+", Y:"+y+", Z:"+z);
        }
    }
}
