package speechless.com;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;

public class PlayerAssignWork implements Listener {
    private int range;
    private Location loc;
    private World world =Bukkit.getWorld("world");
    PlayerAssignWork(int r){
        this.range = r;
        this.loc = world.getSpawnLocation();
    }
    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event){
        if(event.getPlayer().isOp()) return ;
        if(main.board.getObjective("player_score").getScore(event.getPlayer().getName()).getScore() != 0) return;
        int r_x = (int)loc.getX() + (int)(Math.random()*range) - range/2;
        int r_z = (int)loc.getZ() +(int)(Math.random()*range) - range/2;
        int r_y =66;
        int j =0;
        if(world.getBlockAt(r_x,r_y,r_z).getBlockData().getMaterial() != Material.AIR) {
            while (world.getBlockAt(r_x, r_y, r_z).getBlockData().getMaterial() != Material.AIR&&
                    world.getBlockAt(r_x, r_y+1, r_z).getBlockData().getMaterial() != Material.AIR) r_y++;

        }
        else{
            while (world.getBlockAt(r_x, r_y, r_z).getBlockData().getMaterial() != Material.AIR&&
                    world.getBlockAt(r_x, r_y-1, r_z).getBlockData().getMaterial() != Material.AIR) r_y--;
            r_y--;
        }
        Location spawn_loc = new Location(world,r_x,r_y,r_z) ;
        event.getPlayer().teleport(spawn_loc);

    }
}
