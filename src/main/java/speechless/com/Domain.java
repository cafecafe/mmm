package speechless.com;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

public class Domain {
    public static long pre_time;
    public static int player_max_domain;
    public static int expose_rest;
    public static int prepare_period;
    public static int gasDmg;
    public static boolean spiral_task;
    static private FileConfiguration yml ;
    public static int limit_size ;
    public static int total_range ;
    public static int div;
    static int period ;
    static final Location center_loc = Bukkit.getWorld("world").getSpawnLocation();
    static Plugin plugin = Bukkit.getPluginManager().getPlugin("speechless.com");
    static int time_per_turn;
    static int  time_per_shrink ;
    static int shrink_num ;
    static int total_per_turn;
    Domain(main plugin){
        yml = plugin.getConfig();
        player_max_domain = yml.isInt("player_max_domain") ? yml.getInt("player_max_domain") : 4;
        expose_rest = yml.isInt("expose_rest") ? yml.getInt("expose_rest") : 60;
        period = yml.getInt("period");
        limit_size = yml.getInt("limit_size");
        total_range =  yml.getInt("total_range");
        div =  yml.getInt("div");
        time_per_turn =  yml.getInt("period_per_turn")*period;
        time_per_shrink =  yml.getInt("period_per_shrink")*period;
        prepare_period = yml.isInt("prepare_period") ? yml.getInt("prepare_period")*period : 0;
        shrink_num = yml.getInt("shrink_num");
        gasDmg = yml.isInt("gasDmg") ?yml.getInt("gasDmg") : 10;
        total_per_turn = time_per_turn+ time_per_shrink*shrink_num;
        spiral_task = yml.isBoolean("spiral_task");
//        pre_time = yml.isInt("pre_period") ? yml.getInt("pre_period")*period: period;
    }
}
