package speechless.com;

import org.bukkit.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.scoreboard.DisplaySlot;

import java.util.HashMap;
import java.util.Map;

class PlayerNetherAssignWork implements Listener {
    private int range;
    private Map<String,Location> portal = new HashMap<>();
    PlayerNetherAssignWork(int range){
        this.range = range;
    }
    @EventHandler
    public void onPlayerPortalEvent(PlayerPortalEvent event){
        if(!event.getTo().getWorld().getName().equals("world_nether")) return;
        event.getPlayer().sendMessage(event.getTo().getWorld().getName());
        World nether = event.getTo().getWorld();
        Location loc = event.getTo();
        event.getPlayer().setScoreboard(main.board);
        main.board.getObjective("player_score").setDisplaySlot(DisplaySlot.PLAYER_LIST);
        int y =31; int x=0;int z=0;
//        if(event.getPlayer().isOp()) return ;
        do {
            x = (int) loc.getX() + (int) (Math.random() * range) - range / 2;
            z = (int) loc.getZ() + (int) (Math.random() * range) - range / 2;
            while(nether.getBlockAt(x,y+1,z).getBlockData().getMaterial() != Material.AIR
                    || nether.getBlockAt(x,y+2,z).getBlockData().getMaterial() != Material.AIR
                    && y<127){
                y+=2;
            }
            if( y>=127) y =31;
        }while(nether.getBlockAt(x,y,z).getBlockData().getMaterial() == Material.LAVA
                || nether.getBlockAt(x,y,z).getBlockData().getMaterial() == Material.AIR
                || nether.getBlockAt(x,y+1,z).getBlockData().getMaterial() != Material.AIR
                || nether.getBlockAt(x,y+2,z).getBlockData().getMaterial() != Material.AIR);
        new Task.PlayerNetherAssignTask(event.getPlayer(),x,y+1,z);
        event.getPlayer().sendMessage("一股未知的力量使傳送點偏離，你得找出傳送門才能離開");
        event.getPlayer().sendMessage("偵測到傳送門的能量方向!座標: ");
        event.getPlayer().sendMessage("x : "+loc.getX());
        event.getPlayer().sendMessage("z : "+loc.getZ());
        event.getPlayer().setPlayerListFooter(ChatColor.LIGHT_PURPLE+"Origin portal position: \n"
                +ChatColor.DARK_AQUA+"x:"+loc.getX()+", Z:"+loc.getZ()
                +ChatColor.YELLOW+"\n Good Luck!");
        portal.put(event.getPlayer().getName(), loc);
    }
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        if(!event.getPlayer().getWorld().getName().equals("world_nether")) return;
        Bukkit.getLogger().info("aaa");
        Location loc = portal.get(event.getPlayer().getName());
        if(loc == null) return;
        event.getPlayer().setScoreboard(main.board);
        event.getPlayer().setPlayerListFooter(ChatColor.LIGHT_PURPLE+"Origin portal position: \n"
                +ChatColor.DARK_AQUA+"x:"+loc.getX()+", Z:"+loc.getZ()
                +ChatColor.YELLOW+"\n Good Luck!");
    }


}
