package speechless.com;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Objective;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Task {
    static private Plugin plugin = Bukkit.getPluginManager().getPlugin("speechless.com");
    static private Location loc =  Bukkit.getWorld("world").getSpawnLocation();
    static Map<String , Task.SpiralBuildTask> SpiralMap = new HashMap<>();
    static SpiralBuildTask spiralBuildTask = new SpiralBuildTask();
    static CircuitShrink circuitShrink ;
    static final long  tick = 20L ;
    public static class PlayerInvadeTask extends BukkitRunnable {
        private final domainBlock Block ;
        private int range = MyListener.domainBlockListener.one_size/2;
        private boolean expose_signal = false;
        PlayerInvadeTask(domainBlock block)  {
            this.Block = block;
            this.runTaskTimer(plugin,tick,tick);
        }
        public  boolean setExposeSignal( boolean b){
            return  expose_signal = b;
        }
        public void run(){
            if(domainBlock.used_size() > Domain.limit_size) return;
            Block.renderBlockTeam();
            for (Player man: Bukkit.getOnlinePlayers()
                 ) {
                if(Block.getPoint(man) >500 &&!expose_signal){
                    expose_signal =true;
                    new PlayerLocExposeTask(man, Block,this);
                }
                if(!man.getWorld().getName().equals("world")) continue;
                if(Block.isOnRange(man.getLocation())){
                    Block.detectFooterDisplay(man);
                }
                if(!Block.isOnRange(man.getLocation()) || man.isDead()||!Block.isAfford(man)){
                    Block.setPoint(man.getName(),0);
                }
                if(!Block.isOnRange(man.getLocation()) || man.isDead()){
                    Block.clearMember(man.getName());
                }
                else{
                    int temp = Block.getPoint(man);
                    int d =Block.getCenterDistance(man.getLocation());
                    if(d >range*0.5)
                    {
                        temp++;
                    }
                    else if(d >range*0.125)
                    {
                        temp+=2;
                    }
                    else
                    {
                        temp+=3;
                    }
                    Block.setPoint(man.getName(),temp);
                    if(temp >= 1000){
                        Block.setOwner(man);
                        Block.setPoint(man.getName(),0);
                        Block.spawnFireworks(man.getLocation(),3);

                    }
                }

            }
        }
    }

    public static class PlayerOfflinePenaltyTask extends BukkitRunnable {
        private final Objective board_obj;
        PlayerOfflinePenaltyTask(Objective obj){
            this.runTaskTimer(plugin,tick*36,tick*36);
            board_obj = obj;
        }
        public void run() {
            for (OfflinePlayer player: Bukkit.getServer().getOfflinePlayers()
                 ) {
                int temp  =board_obj.getScore(player).getScore();
                if (!player.isOnline() && temp>0) {
                    board_obj.getScore(player).setScore(temp - 1);
                }
            }

        }
    }

    public static class PlayerScoreBoardTask extends BukkitRunnable  {
        private Player man ;
        private final Objective board_obj ;
        PlayerScoreBoardTask(Objective obj,Player player){
            this.runTaskTimer(plugin,tick*10,tick*10);
            this.board_obj = obj;
            this.man = player;
        }
        public void run(){
    //        board_obj.setDisplaySlot(DisplaySlot.PLAYER_LIST);
//            Player[] players = Bukkit.getOnlinePlayers().toArray(new Player[0]);
            if(!man.isOnline()) this.cancel();
            int temp =0 ;
            String name ;
            name = man.getName();
            temp = board_obj.getScore(name).getScore();
            if(temp<0) {
                board_obj.getScore(name).setScore(0);
                MyListener.playerLastLogOutTime.put(man.getName(),man.getPlayerTime());
                man.kickPlayer("your score is too low! \nYou must wait for 30 minutues");
            }
            if(TeamManagement.isTeamMember(man.getName())){
                String team_id = TeamManagement.getTeamId(man.getName());
                int team_size = TeamManagement.board.getTeam(""+team_id).getSize();
                int player_size = Bukkit.getServer().getOnlinePlayers().size();
                temp += 0.5 + (float)player_size/(float)team_size*0.5;
            }
            else{
                int player_size = Bukkit.getServer().getOnlinePlayers().size();
                temp += player_size;
            }
            board_obj.getScore(name).setScore(temp);
            for(domainBlock block : domainBlock.Domains){
                block.setBoard_score(man.getName(),temp);
            }
        }

    }

    public static class InvitationClearTask extends BukkitRunnable {
        private int day;
        InvitationClearTask(int day){
            this.day = day;
            long wait = 86400000 - (new Date().getTime() -7200000)%86400000;
            this.runTaskTimer(plugin,wait,tick*86400*day);
        }
        public void run() {
            TeamManagement.clearRequest();
        }
    }

    public static class PlayerNetherAssignTask extends BukkitRunnable {
        private int x;
        private int y;
        private int z;
        private Player player;
        PlayerNetherAssignTask(Player player,int x, int y, int z){
            this.runTaskLater(plugin,60L);
            this.x = x;
            this.y = y;
            this.z = z;
            this.player = player;
        }
        public void run() {
            World nether = Bukkit.getWorld("world_nether");
            Location set_loc = new Location(nether,x,y,z) ;
            player.getPlayer().teleport(set_loc);
            this.cancel();
        }
    }

    public static class SpiralBuildTask  extends BukkitRunnable{
        private int step =0;
        private int spawn_max = 1;
        private  Location loc;
        private int direct = 0;
        private double d = Domain.total_range;
        private Long end ;
        private static boolean signal = false ;
        private int prepare_period = Domain.prepare_period;
        private boolean prepare = true;
        SpiralBuildTask() {
            if(!Domain.spiral_task) this.cancel();
            loc = Domain.center_loc.clone();
//            Bukkit.getPluginManager().registerEvents(
//                    new MyListener.domainBlockListener(loc ),
//                    plugin);
            this.runTaskTimer(plugin, tick*prepare_period, tick * Domain.total_per_turn);
            end = new Date().getTime() + Domain.time_per_turn*1000;
            new BukkitRunnable(){
                @Override
                public void run() {
                    prepare = false;
                }
            }.runTaskLater(plugin,tick*prepare_period);
        }
        public boolean getSignal(){
            return signal;
        }
        public static void setSignal(){
             signal = true;
        }
        public void Sign(Player player){
            long time = new Date().getTime();
            int day = (int)((end - time)/86400000)+1;
//            player.sendMessage(ChatColor.YELLOW+"Poison gas will dissipate after less than "+d+" midnights!");
            if(prepare) {
                player.sendMessage(ChatColor.GREEN + "毒氣不到" + ((prepare_period/86400)+1)+ "個午夜後消散!");
            }
            else{
                player.sendMessage(ChatColor.GREEN + "毒氣不到" + day + "個午夜後消散!");
            }
        }
        public void run(){
            prepare_period =0;
            signal = false;
            end = new Date().getTime() + Domain.time_per_turn*1000;
            Location temp_loc = loc.clone();
            step++;
            if(direct % 4 == 0){
                loc.setX(temp_loc.getX()+d);
            }
            if(direct % 4 == 1){
                loc.setZ(temp_loc.getZ()+d);
            }
            if(direct % 4 == 2){
                loc.setX(temp_loc.getX()-d);
            }
            if(direct % 4 == 3){
                loc.setZ(temp_loc.getZ()-d);
            }
            if(step == spawn_max){
                if(direct % 2 == 1 ){
                    spawn_max++;
                }
                direct++;
                step =0;
            }
            Bukkit.getPluginManager().registerEvents(
                    new MyListener.domainBlockListener(loc),
                    plugin);
            circuitShrink = new Task.CircuitShrink(loc);

        }
    }
    public static class CircuitShrink extends BukkitRunnable{
        private Location loc;
        private double d ;
        private double shrink;
        private Long end ;
        private final Location center_loc = Domain.center_loc;
        CircuitShrink(Location loc){
            if(!Domain.spiral_task) this.cancel();
            double dx = Math.abs(loc.getX() - center_loc.getX()) ;
            double dz = Math.abs(loc.getZ() - center_loc.getZ());
            d = (int)Math.max(dx,dz) + Domain.total_range/2;
            this.loc = loc;
            this.shrink = (d - Domain.total_range/2)/Domain.shrink_num;
            this.loc.getWorld().getWorldBorder().setCenter(loc.getX(),loc.getZ());
            this.loc.getWorld().getWorldBorder().setDamageAmount(Domain.gasDmg);
            this.loc.getWorld().setSpawnLocation(loc);
            this.runTaskTimer(plugin,0,tick*Domain.time_per_shrink);
            end = (Domain.total_per_turn - Domain.time_per_turn)*1000 + new Date().getTime();
            SpiralBuildTask.setSignal();

        }
        public void detectOuterBorder(Player player){
            double x = player.getLocation().getX();
            double z = player.getLocation().getZ();
            int r = Domain.total_range/2;
            double p = Math.sqrt(Math.pow(x-loc.getX(), 2) + Math.pow(z - loc.getZ(), 2));
            long time = new Date().getTime();
            int day = (int)((end - time)/86400000)+1;
            Bukkit.getLogger().info(""+p);
            if(r <= p){
//                player.sendMessage(ChatColor.YELLOW+"Place outer the next square border will be occupied by poison gas on this midnight");
//                player.sendMessage(ChatColor.RED+"Arrive that before square shrink!");
//                player.sendMessage(ChatColor.YELLOW+"The next square center is on X:"+loc.getX()+", Z:"+loc.getZ());
//                player.sendMessage(ChatColor.YELLOW+"The distance between center and you need less than"+d+" to promise your safe");
                player.sendMessage(ChatColor.YELLOW+"你的所在地今晚將被毒氣入侵");
                player.sendMessage(ChatColor.RED+"你必須在這之前達到安全地點!");
                player.sendMessage(ChatColor.YELLOW+"請前往方向 X:"+loc.getX()+", Z:"+loc.getZ());
                player.sendMessage(ChatColor.YELLOW+"於座標點的距離需小於"+(int)r+"個方格");
                player.sendMessage(ChatColor.AQUA+"距離還有"+(int)(p-r)+"個方格");
            }
            else{
                player.sendMessage(ChatColor.GREEN+"你目前處在安全範圍，毒氣將在不到"+day+"天完全侵犯");
            }
        }
        @Override
        public void run() {
            if(d <= Domain.total_range/2) this.cancel();
            loc.getWorld().getWorldBorder().setSize(d*2);
            d -= shrink;
        }
    }
    public static class PlayerLocExposeTask  extends BukkitRunnable {
        private Player player ;
        private domainBlock block;
        private Task.PlayerInvadeTask task;
        PlayerLocExposeTask(Player player, domainBlock block,Task.PlayerInvadeTask task){
            this.player = player;
            this.block = block;
            this.task = task;
            this.runTaskTimer(plugin,0,Domain.expose_rest*tick);
        }
        @Override
        public void run() {
            Location loc = player.getLocation();
            if(!block.isOnRange(loc) || !block.isAfford(player)){
                task.setExposeSignal(false);
                this.cancel();
            }
            else{
                block.sendPosition(player);
            }
        }
    }
}
