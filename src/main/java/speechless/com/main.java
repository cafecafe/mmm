package speechless.com;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.WorldBorder;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import table.Table;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


public class main extends JavaPlugin {
    public static Connection connection;
    public static Table.Block_Table block_table;
    public static Table.Score_Table score_table;
    static Scoreboard board ;
    static Map<String,Task> TaskMap = new HashMap<>();
    String host, port, database, username, password;

    public Connection openConnection() throws SQLException, ClassNotFoundException {
        if (connection != null && !connection.isClosed()) {
            return null;
        }
        Class.forName("com.mysql.jdbc.Driver");
        return connection = DriverManager.getConnection("jdbc:mysql://"
                        + this.host + ":" + this.port + "/" + this.database,
                this.username, this.password);
    }

    public void onEnable(){
        saveDefaultConfig();
        setMysqlConfig();
        CreateTable();
        new Domain(this);
        board = Bukkit.getScoreboardManager().getMainScoreboard();
        getLogger().info("open......");
        getLogger().info("Hello World!!!!!!");
        setWorldBorder();
        getServer().setIdleTimeout(10);
        createBoardObjective();
        createTeam();
        runTask();
        registerCommand();
        getServer().getPluginManager().registerEvents(new MyListener.playScoreListener(board.getObjective("player_score")),this);
//        getServer().getPluginManager().registerEvents(new PlayerAssignWork(Domain.total_range),this);
        getServer().getPluginManager().registerEvents(new PlayerNetherAssignWork(1500),this);
        getServer().getPluginManager().registerEvents(new MyListener.PlayerBorderWarnListener(),this);
    }

    private void CreateTable() {
        block_table =new Table.Block_Table();
        score_table = new Table.Score_Table();
    }

    private void setMysqlConfig() {
        host = getConfig().getString("msql.host");
        getLogger().info(host);
        port = getConfig().getString("msql.port");
        database = getConfig().getString("msql.database");
        username = getConfig().getString("msql.username");
        password = getConfig().getString("msql.password");
        try {
            openConnection();
        }
        catch (SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static void createBoardObjective(){
        try {
            board.registerNewObjective("player_score","dummy","player_score").setDisplaySlot(DisplaySlot.PLAYER_LIST);
        } catch (IllegalArgumentException e){
        };
        for (Player player: Bukkit.getOnlinePlayers()
        ) {
            new Task.PlayerScoreBoardTask(board.getObjective("player_score"),player);
        }

    }
    public static void createTeam(){
        try{
            board.registerNewTeam("default").setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.NEVER);
        }catch(IllegalArgumentException e){
        }
        for (Player player: Bukkit.getOnlinePlayers()
        ) {
            TeamManagement.initCreate(player.getName());
            board.getTeam("default").addEntry(player.getName());
        }
    }
    public void setWorldBorder(){
        WorldBorder border = Bukkit.getWorld("world").getWorldBorder();
        border.setSize(Domain.total_range);
        border.setCenter(Domain.center_loc);
        border.setDamageAmount(Domain.gasDmg);
        Bukkit.getPluginManager().registerEvents(
                new MyListener.domainBlockListener(Domain.center_loc ),
                this);
    }
    public  void runTask(){
//        new Task.PlayerOfflinePenaltyTask(board.getObjective("player_score"));
        new Task.InvitationClearTask(1);
    }
    public void registerCommand(){
        this.getCommand("/domain").setExecutor(new Command.ShowDomain());
        this.getCommand("/team").setExecutor(new Command.ShowTeam());
        this.getCommand("/quit").setExecutor(new Command.QuitTeam());
        this.getCommand("/invite").setExecutor(new Command.Invite());
//        this.getCommand("/create").setExecutor(new Command.CreateTeam());
        this.getCommand("/reset").setExecutor(new Command.ResetScoreboard());
        this.getCommand("/agree").setExecutor(new Command.AgreedTeam());
        this.getCommand("/send").setExecutor(new Command.Send());
        this.getCommand("/spiral_reset").setExecutor(new Command.ResetSpiral());

    }
    public void onDisable(){
        resetDomainObject();
        resetTeam();
        reScore();
        Bukkit.getLogger().info("close......");
    }

    private void reScore() {
        Objective obj = board.getObjective("player_score");
        for (Player man: Bukkit.getOnlinePlayers()
        ) {
            int i = obj.getScore(man.getName()).getScore();
            score_table.setScore(man.getName(),i);
        }
        for (Player man: Bukkit.getOnlinePlayers()
        ) {
            int i = score_table.getScore(man.getName());
            obj.getScore(man.getName()).setScore(i);
        }
    }


    public void onLoad(){
    }
    public static void resetBoardObjective(){
        board.getObjective("player_score").unregister();
    }
    public void resetDomainObject(){
        for (Objective obj: Bukkit.getScoreboardManager().getMainScoreboard().getObjectives()
             ) {
            String temp = obj.getName();
            if(temp.startsWith("domain")){
                obj.unregister();
            }
        }
    }
    public  static void resetTeam(){
        Team[] teams = board
                .getTeams().toArray(new Team[0]);
        for(Team team : teams){
            team.unregister();
        }

    }

}