package speechless.com;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.*;


public class TeamManagement {
    static int TeamId = 0;
    static private Map<String,String> InviteRequest = new HashMap<>();
    static private Map<String,String> Members = new HashMap<>();
    static Scoreboard board =    main.board;
    static public void registerTeam(String player){
        if(board.getTeam(""+TeamId) == null) {
            board.registerNewTeam("" + TeamId).addEntry(player);
            board.getTeam(""+TeamId).setOption(Team.Option.NAME_TAG_VISIBILITY,Team.OptionStatus.NEVER);
        }
        else board.getTeam(""+TeamId).addEntry(player);
        Members.put(player,""+TeamId);
        TeamId++;
    }
    static public boolean isSameTeam(String p1, String p2){
        if(!isTeamMember(p1)||!isTeamMember(p2)){
            return false;
        }
        return Members.get(p1) == Members.get(p2);

    }
    static public void invitePlayer(String to,String from){
        InviteRequest.put(to, from);
    }
    static public void agreedInvitation(String to,String from){
        if(InviteRequest.containsValue(from)){
            String id = Members.get(from);
            Members.put(to,id);
            board.getTeam(""+id).addEntry(to);
            board.getTeam(""+id).setPrefix(ChatColor.GRAY+""+id+" ");
            Bukkit.getPlayer(to).setDisplayName(ChatColor.YELLOW+id+":"+ChatColor.WHITE+to);
            InviteRequest.remove(to);
            Bukkit.getPlayer(to).sendMessage(ChatColor.GREEN + to + " agreed!!");
        }
        else{
            Bukkit.getPlayer(to).sendMessage(ChatColor.RED+"You are not invited");
        }
    }
    static public boolean isTeamMember(String player){
        return Members.get(player) != "default"&& Members.get(player)!= null;
    }
    static public void quitTeam(String player){
        String id = Members.get(player);
        for (String e: board.getTeam(""+id).getEntries()
             ) {
            Bukkit.getPlayer(e).sendMessage(ChatColor.RED+player+"  have left from team");
        }
        board.getTeam("default").addEntry(player);
        Members.put(player,"default");
        Bukkit.getPlayer(player).setDisplayName("");
        Bukkit.getPlayer(player).setDisplayName(player);
        if(board.getTeam(id).getSize() ==0) board.getTeam(id).unregister();
    }
    static public void showTeam(String player){
        String id = Members.get(player);
        Bukkit.getPlayer(player).sendMessage("TeamList:");
        for (String e: board.getTeam(""+id).getEntries()
        ) {
            Bukkit.getPlayer(player).sendMessage(e);
        }
    }
    static public String getSender(String name){
        return InviteRequest.get(name);
    }
    static public String getTeamId(String player){
        return Members.get(player);
    }
    static public void clearRequest(){
        InviteRequest.clear();
    }
    static public void initCreate(String name){
        TeamManagement.Members.put(name,"default");
    }

}
