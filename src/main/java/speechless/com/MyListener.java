package speechless.com;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockCanBuildEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.Objective;

import java.util.HashMap;
import java.util.Map;

public class MyListener{
    static Map<String,Long> playerLastLogOutTime = new HashMap<>();
    public static class  domainBlockListener  implements Listener{
        private final domainBlock[] Block ;
        private final int div;
        private final double base_x;
        private final double base_z;
        private final int range;
        static int one_size;
        private World world =Bukkit.getServer().getWorld("world");
        domainBlockListener(Location loc){
            this.div = Domain.div;
            this.range = Domain.total_range;
            Block = new domainBlock[div*div];
            this.one_size = range / div;
            double world_center_x = loc.getX();
            this.base_x = world_center_x - range/2 ;
            double d1 = this.base_x + one_size/2;
            double world_center_z = loc.getZ();
            this.base_z = world_center_z -range/2;
            double d2 = this.base_z +one_size/2;
            for (int i = 0; i < div; i++) {
                for (int j =0; j < div; j++) {
                    Block[i*div+j] = new domainBlock(
                            i*one_size+d1
                            ,j*one_size+d2,one_size);
                    new Task.PlayerInvadeTask(Block[i*div+j] );
                }

            }
        }
        @EventHandler
        public void onPlayerMoveEvent(PlayerMoveEvent event) {
            if(!event.getPlayer().getWorld().getName().equals("world")) return;
            Location loc = event.getTo();
            domainBlock to_block = getBlock(event.getTo());
            domainBlock from_block = getBlock(event.getFrom());
            if(to_block == null){
                if(from_block != null){
                    event.getPlayer().setPlayerListFooter(ChatColor.GRAY+"Outer Domain");
                    event.getPlayer().setScoreboard(main.board);
                }
                return;
            }
            try {
                if(!to_block.equals(from_block)){
                    to_block.detectSendMs(event.getPlayer());
                    to_block.detectFooterDisplay(event.getPlayer());
                }
            }
            catch (NullPointerException e){
                return;
            }
        }
        @EventHandler
        public void onBlockBreakEvent(BlockBreakEvent event) {
            if(!event.getPlayer().getWorld().getName().equals("world")) return;
            Location loc = event.getBlock().getLocation();
            domainBlock block = getBlock(loc);
            if(block == null) return;
            try {
                block.BlockRecover(event,loc.getBlock());
            }catch(NullPointerException e){
                return;
            }
        }
        @EventHandler
        public void onBlockCanBuildEvent(BlockCanBuildEvent event) {
            if(!event.getPlayer().getWorld().getName().equals("world")) return;
            Location loc = event.getBlock().getLocation();
            domainBlock block = getBlock(loc);
            if(block == null) return;
            try{
                block.BlockRemove(event.getPlayer() ,event.getBlock().getLocation());
            }
            catch(NullPointerException e){
                return;
            }
        }
        @EventHandler
        public void onPlayerDeath(PlayerDeathEvent event){
            Location loc = event.getEntity().getLocation();
            domainBlock.clearPlayerDomain(event.getEntity().getName());

        }
        @EventHandler
        public void onPlayerQuit(PlayerQuitEvent event){
            if(!event.getPlayer().getWorld().getName().equals("world")) return;
            String player = event.getPlayer().getName();
            Location loc = event.getPlayer().getLocation();
            domainBlock block = getBlock(loc);
            if(block == null) return;
            try{
                getBlock(loc).setPoint(player,0);
                getBlock(loc).clearMember(player);
            }
            catch(NullPointerException e){
                return;
            }
//           getBlock(loc).clearMember(event.getPlayer().getName());
        }
        @EventHandler
        public void onPlayerJoin(PlayerJoinEvent event) {
            if(!event.getPlayer().getWorld().getName().equals("world")) return;
            Location loc = event.getPlayer().getLocation();
            try{
                domainBlock block = getBlock(loc);
                if(getBlock(loc) == null){
                    return;
                }
                else block.detectSendMs(event.getPlayer());
            }
            catch( NullPointerException e){
                return;
            }
        }
        public domainBlock getBlock(Location loc){
            int i,j;
            if(loc.getX() - base_x >0){
                i = (int)((loc.getX() - base_x) / one_size);
            }
            else{
                i = (int)((loc.getX() - base_x) / one_size)-1;
            }
            if(loc.getZ() - base_z >0){
                j = (int)((loc.getZ() - base_z) / one_size);
            }
            else{
                j = (int)((loc.getZ() - base_z) / one_size)-1;
            }
            if(!loc.getWorld().equals(world)) return null;
            if(i <0 || j<0 || i>=div || j >=div) return null;
            try {
                return Block[i * div + j];
            }
            catch (ArrayIndexOutOfBoundsException e){
                return null;
            }
        }

    }
    public static class playScoreListener  implements Listener{

        static   Objective board_obj;
        playScoreListener(Objective player_board) {
            this.board_obj = player_board;
        }

        @EventHandler
        public void onPlayerJoin(PlayerJoinEvent event) {
            event.getPlayer().addAttachment(Bukkit.getPluginManager().getPlugin("AdvancedAchievements"),"bukkit.command.aach.*",false);
            if(!event.getPlayer().getWorld().getName().equals("world")) return;
            Bukkit.broadcastMessage(event.getPlayer().getName()+",Welcome!^_^");
            Player man = event.getPlayer();
            if(!TeamManagement.isTeamMember(man.getName()))
                TeamManagement.initCreate(man.getName());
            new Task.PlayerScoreBoardTask(main.board.getObjective("player_score"),man);
            man.setPlayerListHeader(ChatColor.GREEN+"Survival Point");
            board_obj.getScore(man.getName()).setScore(main.score_table.getScore(man.getName()));
//            if(playerLastLogOutTime.containsKey(man.getName())){
//                long time = man.getPlayerTime() - MyListener.playerLastLogOutTime.get(man.getName());
//                Bukkit.broadcastMessage(""+man.getPlayerTime() );
//                Bukkit.broadcastMessage(""+MyListener.playerLastLogOutTime.get(man.getName()));
//                if(time < Penalty.left_wait_sec) man.kickPlayer("Waiting for "+(Penalty.left_wait_sec - time/20L)+" seconds,please!");
//                else {
//                    playerLastLogOutTime.remove(man.getName());
//                    board_obj.getScore(man).setScore(0);
//                }
//            }

        }
        @EventHandler
        public void onPlayerDeath(PlayerDeathEvent event){
            int temp = board_obj.getScore(event.getEntity().getName()).getScore();
            board_obj.getScore(event.getEntity().getName()).setScore(0);
        }
        @EventHandler
        public  void onPlayerQuitEvent(PlayerQuitEvent event){
            String name = event.getPlayer().getName();
            int i = board_obj.getScore(name).getScore();
            main.score_table.setScore(name,i);
        }
    }

    public static class PlayerBorderWarnListener implements Listener {
        @EventHandler
        public  void onPlayerJoinEvent(PlayerJoinEvent event){
            if(!event.getPlayer().getWorld().getName().equals("world")) return;
            if(Task.spiralBuildTask.getSignal()){
                Task.circuitShrink.detectOuterBorder(event.getPlayer());
            }
            else Task.spiralBuildTask.Sign(event.getPlayer());
        }
    }
}
