package speechless.com;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.scheduler.BukkitRunnable;

public class RemoveBlockTask  extends BukkitRunnable {
    private Location loc;
    RemoveBlockTask(Location loc){
        this.loc = loc;
        this.runTaskLater(Domain.plugin,1L);
    }
    @Override
    public void run() {
        loc.getBlock().setType(Material.AIR);
    }
}
